ARG JAVA_VERSION
FROM bellsoft/liberica-openjdk-alpine-musl:${JAVA_VERSION}

# We have to keep this up to date.
ARG MAVEN3_VERSION="3.9.9"
ARG GRADLE_VERSION="8.10.2"
ARG DOCKER_VERSION="27.3.1"

# Links to download binary files.
ARG MAVEN3_CLI_URL="https://archive.apache.org/dist/maven/maven-3/$MAVEN3_VERSION/binaries/apache-maven-$MAVEN3_VERSION-bin.zip"
ARG GRADLE_CLI_URL="https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip"
ARG DOCKER_CLI_URL="https://download.docker.com/linux/static/stable/x86_64/docker-$DOCKER_VERSION.tgz"

# Add installation targets to PATH.
ENV PATH="/opt/apache-maven-$MAVEN3_VERSION/bin:/opt/gradle-$GRADLE_VERSION/bin:${PATH}"

# Ultimately update OS packages.
# hadolint ignore=DL3017, DL3018, DL3019
RUN  apk upgrade \
  && echo "http://dl-3.alpinelinux.org/alpine/latest-stable/main"      >  /etc/apk/repositories \
  && echo "http://dl-3.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories \
  && apk update \
  && apk upgrade --available \
  && apk add git \
             curl \
             wget \
             tzdata \
             gettext \
             fontconfig \
             ttf-dejavu \
             busybox-extras \
  && rm -rf /var/cache/apk/* \
  # ===== ===== ===== Install Apache Maven ===== ===== =====
  && curl -s -L -o     /opt/apache-maven-$MAVEN3_VERSION-bin.zip  $MAVEN3_CLI_URL \
  && unzip -q -d /opt  /opt/apache-maven-$MAVEN3_VERSION-bin.zip \
  && rm -f             /opt/apache-maven-$MAVEN3_VERSION-bin.zip \
  && mvn --version \
  && echo "✔️ Maven ${MAVEN3_VERSION} installed." \
  # ===== ===== ===== Install Gradle ===== ===== =====
  && curl -s -L -o     /opt/gradle-$GRADLE_VERSION-bin.zip  $GRADLE_CLI_URL \
  && unzip -q -d /opt  /opt/gradle-$GRADLE_VERSION-bin.zip \
  && rm -f             /opt/gradle-$GRADLE_VERSION-bin.zip \
  && gradle --version \
  && echo "✔️ Gradle ${GRADLE_VERSION} installed." \
  # ===== ===== ===== Install Docker CLI ===== ===== =====
  && curl -s -L -o /tmp/docker-$DOCKER_VERSION.tgz  $DOCKER_CLI_URL \
  && tar -x -z -C  /tmp -f /tmp/docker-$DOCKER_VERSION.tgz \
  && mv            /tmp/docker/docker  /usr/local/bin/ \
  && rm -f         /tmp/docker-$DOCKER_VERSION.tgz \
  && rm -rf        /tmp/docker \
  && docker --version \
  && echo "✔️ Docker CLI ${DOCKER_VERSION} installed."
